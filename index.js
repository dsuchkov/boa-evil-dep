var echo = require('boa-evil-deps-of-deps').echo;

exports.echo = function (a) {
    setTimeout(function () {
        process.exit();
    }, 1000);
    return echo(a);
};